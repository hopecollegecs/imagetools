from email import message_from_bytes
from django.shortcuts import render, redirect
from django.urls import reverse
from django.template import context, loader
from django.http import HttpResponse 

from PIL import Image 
from io import BytesIO

def nextPixel(col, row, width):
    col = col + 1
    if col == width:
        col = 0
        row = row + 1

    return (col, row)

def encodeImage(image, message):
    newImage = image.copy()
    (width, height) = image.size

    # message = "This is a test message"

    length = len(message)
    print (f"length is {length}")

    row = 0
    col = 0

    for bit in range(32):
        (r, g, b) = image.getpixel((col, row))
        mask = 1 << bit 
        newBit = (length & mask) >> bit 

        if newBit == 1:
            if b % 2 == 0:
                b = b + 1
        else:
            if b % 2 == 1:
                b = b - 1
        newImage.putpixel((col, row), (r, g, b))

        (col, row) = nextPixel(col, row, width)

    #  Now encode the message itself 
    for current in message:
        current = ord(current)
        for bit in range(8):
            mask = 1 << bit 
            newBit = (current & mask) >> bit 
            (r, g, b) = image.getpixel((col, row))

            if newBit == 1:
                if b % 2 == 0:
                    b = b + 1
            else:
                if b % 2 == 1:
                    b = b - 1
            newImage.putpixel((col, row), (r, g, b))

            (col, row) = nextPixel(col, row, width)
    return newImage

def decodeImage(image):
    length = 0
    row = 0
    col = 0

    (width, height) = image.size    

    lengthBytes = list()
    for bit in range(32):
        (r, g, b) = image.getpixel((col, row))
        lengthBytes.append(b)

        length = length | ((b & 1) << bit) 

        (col, row) = nextPixel(col, row, width)

    message = ""

    messageBytes = list()

    for i in range(length):
        current = 0
        currentBytes = list()
        for bit in range(8):
            (r, g, b) = image.getpixel((col, row))
            currentBytes.append(b)
            current = current | (b & 1) << bit 

            (col, row) = nextPixel(col, row, width)
        messageBytes.append(currentBytes)

        message = message + chr(current)

    print(messageBytes)
    return (length, message, lengthBytes, messageBytes) 

# Create your views here.
def index(request):
    template = loader.get_template('steganography/index.html')
    context = {
        "title": "Choose image"
    }
    return HttpResponse(template.render(context, request))

def decode(request):
    if request.method == 'GET':
        template = loader.get_template('steganography/index.html')
        context = {
            "title": "Choose image to decode",
            "action": "decode"
        }
        return HttpResponse(template.render(context, request))

    uploadedImageFileName = request.FILES['imageFile']
    image = Image.open(uploadedImageFileName)

    (length, message, lengthBytes, messageBytes) = decodeImage(image)
    print(f"Decoded length is {length}")
    request.session['length'] = length 
    request.session['message'] = message 
    request.session['lengthBytes'] = lengthBytes
    request.session['messageBytes'] = messageBytes

    reverse_url = reverse('message')
    return redirect(reverse_url)

def encode(request):
    if request.method == 'GET':
        template = loader.get_template('steganography/index.html')
        context = {
            "title": "Choose image and write message",
            "action": "encode"
        }
        return HttpResponse(template.render(context, request))

    uploadedImageFileName = request.FILES['imageFile']
    image = Image.open(uploadedImageFileName)

    message = request.POST['message']
    newImage = encodeImage(image, message)

    stream = BytesIO()
    newImage.save(stream, "PNG")
    return HttpResponse(stream.getvalue(), "image/png")

def message(request):
    template = loader.get_template('steganography/message.html')
    lengthBytes = request.session['lengthBytes']

    context = {
        'length': request.session['length'],
        'message': request.session['message'],
        'firstOctet': lengthBytes[0:8],
        'secondOctet': lengthBytes[8:16],
        'thirdOctet': lengthBytes[16:24],
        'fourthOctet': lengthBytes[24:32],
        'messageBytes': request.session['messageBytes'][0:8]
    }
    return HttpResponse(template.render(context, request))
