from time import time
from django.shortcuts import redirect, render
from django.urls import reverse
from django.http import HttpResponse 
from django.template import context, loader
from django.views.decorators import cache
from django.views.decorators.cache import cache_control, cache_page

from math import ceil
import numpy as np 
import pandas as pd
import json 

from io import BytesIO
from colorsys import rgb_to_hsv, hsv_to_rgb

from PIL import Image
import svgwrite

imageCachePeriodMinutes = 15

# Create your views here.
def index(request):
    template = loader.get_template('imagecolors/index.html')
    context = {
        "title": "Choose image"
    }
    return HttpResponse(template.render(context, request))

#  Cache this request for 15 minutes
@cache_page(60*imageCachePeriodMinutes)
def image(request, imageFileName):
    print("In image")
    imageAsArray = np.asarray(request.session['image'], dtype=np.uint8);
    pilImage = Image.fromarray(imageAsArray, mode="RGB")
    stream = BytesIO()
    pilImage.save(stream, "JPEG")
    return HttpResponse(stream.getvalue(), "image/jpeg")

#  Cache this request for 15 minutes
@cache_page(60*imageCachePeriodMinutes)
def imagesvg(request, imageFileName):
    return HttpResponse(request.session['svg'], "image/svg+xml")

def imagedisplay(request, imageFileName):
    print("In imagedisplay")
    distribution_df = request.session['distribution_df']
    context = {
        "title": "Image Details",
        'df': request.session['df'],
        'distribution_df': distribution_df,
        'first_row': 1,
        'previous_page_classes': ["page-item"],
        'next_page_classes': ["page-item"],
        'imageFileName': imageFileName,
        'imageFormat': request.session['imageFormat'],
        'imageWidth': request.session['imageSize'][0],
        'imageHeight': request.session['imageSize'][1],
        'pixels': request.session['imageSize'][0] * request.session['imageSize'][1],
        'uniqueColors': request.session['uniqueColors']
    }

    page_size = request.GET.get('page_size', 10)
    page = int(request.GET.get('page', 0))

    
    # page = int(request.GET.get('page'))
    total_pages = ceil(len(distribution_df) / page_size)
    print(f"Total pages is {total_pages} for {len(distribution_df)} colors")
    slice_start = page * page_size
    slice_end = min((page+1)*page_size, len(distribution_df))
    context['distribution_df'] = distribution_df[slice_start:slice_end]
    context['first_row'] = slice_start

    if page == 0:
        context['previous_page_classes'].append("disabled")
    elif page == total_pages - 1:
        context['next_page_classes'].append("disabled")

    context['page'] = page
    
    context['previous_page_classes'] = " ".join(context['previous_page_classes'])
    context['next_page_classes'] = " ".join(context['next_page_classes'])


    return render(request, 'imagecolors/imagedisplay.html', context);

def to_rgb(r):
    rgb_float = hsv_to_rgb(r['H']/255, r['S']/255, r['L']/255)
    # return ( 
    #     int(rgb_float[0]*255),
    #     int(rgb_float[1]*255),
    #     int(rgb_float[2]*255)
    # )
    return f"{int(rgb_float[0]*255)},{int(rgb_float[1]*255)},{int(rgb_float[2]*255)}" 

def upload(request):
    print("In upload")
    uploadedImageFileName = request.FILES['imageFile']

    image = Image.open(uploadedImageFileName)
    (width, height) = image.size

    print (f"Original Size:\t Width: {width}, Height: {height}")
    ar = width / height 

    start = time()
    if width > 600:
        width = 600
        height = int(width / ar)
        image = image.resize((width, height ))

    if height > 600:
        height = 600
        width = int(ar*height)
        image = image.resize((width, height))

    print (f"Final size:\t Width: {width}, Height: {height}")
    end = time()
    print(f"Resize: {end - start}")

    start = time()
    request.session['image'] = np.asarray(image).tolist()
    request.session['imageSize'] = image.size
    request.session['imageFormat'] = image.format

    rgb_ndarray = np.asarray(image)
    image_shape = rgb_ndarray.shape
    rgb_linear_array = rgb_ndarray.reshape(image_shape[0]*image_shape[1], image_shape[2])

    hsl = image.convert("HSV")
    hsl_ndarray = np.asarray(hsl)
    hsl_linear_array = hsl_ndarray.reshape(image_shape[0]*image_shape[1], image_shape[2])

    df = pd.DataFrame({
        "Hue": hsl_linear_array[:, 0], 
        "Saturation": hsl_linear_array[:, 1],
        "Lightness": hsl_linear_array[:, 2],
        "Red": rgb_linear_array[:, 0],
        "Green": rgb_linear_array[:, 1],
        "Blue": rgb_linear_array[:, 2]
    })

    df['HSL'] = df['Hue'].map(str) + "," + df['Saturation'].map(str) + "," + df['Lightness'].map(str)
    df['RGB'] = df['Red'].map(str) + "," + df['Green'].map(str) + "," + df['Blue'].map(str)

    json_records = df.head().reset_index().to_json(orient ='records')
    data = json.loads(json_records)
    request.session['df'] = data

    end = time()
    print(f"Create df: {end-start}")

    start = time()
    hsl_occurrences = df['HSL'].value_counts()
    rgb_occurrences = df['RGB'].value_counts()

    request.session['uniqueColors'] = len(rgb_occurrences)

    distribution_df = pd.DataFrame({
        'HSL': hsl_occurrences.index,    
        'Count': hsl_occurrences.values,
        'Percentage': hsl_occurrences.values / hsl_linear_array.shape[0]
    })

    hsl_columns = distribution_df['HSL'].str.split(",", expand=True)
    distribution_df['H'] = hsl_columns[0].astype('int32')
    distribution_df['S'] = hsl_columns[1].astype('int32')
    distribution_df['L'] = hsl_columns[2].astype('int32')
    
    distribution_df['RGB'] = distribution_df.apply(to_rgb, axis=1)
    rgb_columns = distribution_df['RGB'].str.split(",", expand=True)
    distribution_df['R'] = rgb_columns[0].astype('int32')
    distribution_df['G'] = rgb_columns[1].astype('int32')
    distribution_df['B'] = rgb_columns[2].astype('int32')

    json_records = distribution_df.head(n=50).reset_index().to_json(orient ='records')
    data = json.loads(json_records)
    request.session['distribution_df'] = data
    end = time()
    print(f"Create sorted_distribution: {end-start}")

    start = time()
    sorted_distribution = distribution_df.sort_values(["H", "S", "L"], axis=0)
    end = time()
    print(f"Sort sorted_distribution: {end - start}")
   
    start = time()
    heightAsPercentOfWidth = 0.15

    drawing = svgwrite.Drawing("drawing.svg", viewBox=f"0 0 1 {heightAsPercentOfWidth}")

    def addRect(j):
        (r, g, b) = (j["R"], j["G"], j["B"])
        r = drawing.rect((j["start"], 0), (j["Percentage"], heightAsPercentOfWidth), fill=svgwrite.rgb(r, g, b))    
        drawing.add(r)

    startAddRec = time()

    #  Start the index at 1 in order to make a place for the starting 0
    sorted_distribution.index = np.arange(1, len(sorted_distribution)+1)

    #  Now compute the cumulative sums, and shift them down by 1 row by placing a 0 at the beginning
    cumsum = sorted_distribution['Percentage'].cumsum()
    zero = pd.Series([0.0])
    startingXCoordinates = pd.concat((zero, cumsum[:-1]), axis=0)

    #  Make sorted_distribution have a 0-based index so that adding the startingXCoordinates series has matches for all index values
    sorted_distribution.reset_index(inplace=True)
    sorted_distribution['start'] = startingXCoordinates 

    sorted_distribution.apply(addRect, axis=1)
    endAddRec = time() 

    print(f"Add rectangles using apply: {endAddRec - startAddRec}")

    request.session['svg'] = drawing.tostring()
    end = time()
    print(f"Create SVG: {end - start}")

    reverse_url = reverse('imagedisplay', kwargs={'imageFileName': uploadedImageFileName})
    return redirect(reverse_url)