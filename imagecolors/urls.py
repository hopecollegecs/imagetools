from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('image/<str:imageFileName>', views.image, name='image'),
    path('imagesvg/<str:imageFileName>', views.imagesvg, name='imagesvg'),
    path('imagedisplay/<str:imageFileName>', views.imagedisplay, name='imagedisplay'),
    # path('image/<str:imageFileName>', views.image, name='image'),
    path('upload', views.upload, name='upload')
]