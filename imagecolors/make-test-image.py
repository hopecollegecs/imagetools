from PIL import Image
import numpy as np 

im = Image.new("RGB", (600, 600))
for x in np.arange(300, 600):
    for y in range(300):
        im.putpixel((x, y), (255, 0, 0))
    for y in np.arange(300, 600):
        im.putpixel((x, y), (0, 255, 0))

for y in np.arange(450, 600):
    for x in np.arange(300, 450):
        im.putpixel((x, y), (0, 0, 255))    
    for x in np.arange(450, 600):
        im.putpixel((x, y), (0, 255, 255))

im.save("testimage.png", "PNG")
