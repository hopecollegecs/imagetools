from django.apps import AppConfig


class ImagecolorsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'imagecolors'
